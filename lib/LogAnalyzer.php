<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
$months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

function hrSize($size) {
    $mods  = ['b', 'Kb', 'Mb', 'Gb', 'Tb'];
    $i = 0;
    while ($size > 1024) {
        $size = $size/1024;
        $i++;
    }

    return number_format($size,2,',','')." $mods[$i]";
}

class DbMapper {
    private $dbName = 'logdb.db';
    private $ldb;
    private $isOpen = false;
    private $ltime = "";

    function __construct() {
        $this->ldb = new SQLite3($this->dbName);
        $this->ldb->exec('pragma synchronous = off;');
    }

    private function lastDate() {
        $res = $this->ldb->query("select max(atime) atime from logs;");
        if ($res != null) $row = $res->fetchArray();
         else return "0000-00-00 00:00:00";

        //foreach($row as $n=>$v) echo "\n".$n."->".$v;
        //echo $row["atime"];
        return $row["atime"];
    }

    private function isInserted($dtime) {
        //init last time
        if ($this->ltime == "") $this->ltime = $this->lastDate();

        //echo "\n".$this->ltime; die('here');

        if (strtotime($this->ltime) >= strtotime($dtime)) return true;
         else return false;
    }

    public function updLog($ftime, $url, $size) {
      $this->ldb->query("update logs set ftime='$ftime', size=$size where (url='$url') and (ftime = '');");
        //Move last time access to current position is less
        if (strtotime($this->ltime) < strtotime($ftime)) $this->ltime = $ftime;
    }

    public function insLog($atime, $ftime, $url, $size, $ip) {
        if (!$this->isOpen) {
            $this->ldb->query("create table logs (atime datetime, ftime datetime,  url text, size numberic, ip varchar);");
            $this->isOpen = true;
        }

        //If this date time is not inserted yet -> current date and time aren't been reached
        if (!$this->isInserted($atime)) {
            $this->ldb->query("insert into logs (atime, ftime, url, size, ip) values ('$atime', '$ftime', '$url', $size, '$ip')");
            return true;
        } else return false;
    }

    public function clearOld($maxDays) {
        if ($maxDays == null) $maxDays = 60;
        $this->ldb->query("delete from logs where atime < datetime('now', 'localtime', '-$maxDays days');");
        return true;
    }

    public function ipSize($ip) {
        $res = $this->ldb->query("select sum(size) as allsize from logs where (ip='$ip') and (ftime != '0000-00-00 00:00:00'); ");
        if ($res != null) $row = $res->fetchArray();
         else return "0";
        return $row["allsize"];
    }

    public function ipTime($ip) {
        $res = $this->ldb->query("select sum(cast((julianday(ftime) - julianday(atime))*24 as REAL)) as alltime from logs where ip='$ip' and (ftime != '0000-00-00 00:00:00'); ");
        if ($res != null) $row = $res->fetchArray();
        else return "0";
        return $row["alltime"];
    }

    public function allSize($order) {
        if (($order == null) || ($order == "")) $order = "-size";
        if (substr($order,0,1) == "-") $sorder = " desc";
        else $sorder = "";
        $order = substr($order,1,strlen($order));

        if ($order == "size") $order = "order by allsize";
        if ($order == "time") $order = "order by alltime";
        if ($order == "ip") $order = "order by ip";
        $order .= $sorder;

        //, sum(cast((julianday(ftime) - julianday(atime))*24 as REAL)) as alltime
        $sql = "select ip, sum(size) as allsize from logs where (ftime != '0000-00-00 00:00:00') group by ip $order;";
        //echo $sql;
        $res = $this->ldb->query($sql);
        $arr = []; $i = 0; $row = null;
        if ($res != null) {
            do {
                if (($row != null) && ($row["ip"] != null)) $arr[$i] = $row;
                $i++;
            } while ($row = $res->fetchArray());
            return $arr;
        }
        else return [];
    }

    public function ipLog($ip, $stime, $etime, $order) {
        $addon = "";

//        strftime('%s', date_field)
        if ($stime != "")
            $addon .= " and (atime >= '$stime')";

        if ($etime != "")
            $addon .= " and (atime <= '$etime')";


        if (($order == null) || ($order == "")) $order = "-size";
        if (substr($order,0,1) == "-") $sorder = " desc";
         else $sorder = "";
        $order = substr($order,1,strlen($order));

        if ($order == "size") $order = "order by size";
        if ($order == "date") $order = "order by atime";
        if ($order == "url") $order = "order by url";
        $order .= $sorder;


        $sql = "select atime, ftime, url, size from logs where (ip='$ip') $addon $order; ";
        $res = $this->ldb->query($sql);
        //echo $sql;

        $arr = []; $i = 0; $row = null;
        if ($res != null) {
            do {
                if ($row != null) $arr[$i] = $row;
                $i++;
            } while ($row = $res->fetchArray());
            //var_dump($arr);
            //echo "<br/>";
          return $arr;
        } else return [];
    }

    public function normalize() {
        $res = $this->ldb->query("select * from logs;");
        $row = null;
        if ($res != null) do {
            if ($row != null) {
                $atime = $row["atime"];
                $ftime = $row["ftime"];
                $url = $row["url"];

                $spltime = explode(" ", $atime);
                $spldate = explode("-", $spltime[0]);

                if (strlen($spldate[1]) == 1) $spldate[1] = "0".$spldate[1];
                if (strlen($spldate[2]) == 1) $spldate[2] = "0".$spldate[2];

                $spltime[0] = $spldate[0]."-".$spldate[1]."-".$spldate[2];
                $modatime = $spltime[0]." ".$spltime[1];

                $spltime = explode(" ", $ftime);
                $spldate = explode("-", $spltime[0]);

                if (strlen($spldate[1]) == 1) $spldate[1] = "0".$spldate[1];
                if (strlen($spldate[2]) == 1) $spldate[2] = "0".$spldate[2];

                $spltime[0] = $spldate[0]."-".$spldate[1]."-".$spldate[2];
                $modftime = $spltime[0]." ".$spltime[1];

                $this->ldb->query("update logs set atime='$modatime', ftime='$modftime' where ".
                    "(atime='$atime') and (ftime='$ftime') and (url='$url'); ");
            }
        } while ($row = $res->fetchArray());
    }

    public function fillEmptyATime() {
        $this->ldb->query("update logs set ftime='0000-00-00 00:00:00', size=0 where (ftime = ''); ");
    }

}
class LogAnalyzer
{
  private $dbMapper;

  function __construct() {
      $this->dbMapper = new DbMapper();
  }

   function analyzeFile($fname) {
       $ins = file_get_contents($fname);
       $inarr = explode("\n", $ins);
       $totRows = 0;

       //Customer variables
       $atime = "";
       $ftime = "";
       $ip = "";
       $url = "";
       $rurl = "";
       $size = "";

       foreach ($inarr as $line) {
           #Month (0)
           #day (1)
           #time (2)
           #srv ip (3)
           #web-proxy,account // web-proxy,debug (4)
           #proxy: (5)
           # IP GET: url // Response to "GET: url"

           $needle_clen = "Content-Length: ";
           global $months;

           //Truncate multiple spaces
           while (strpos($line, "  ") > -1) $line = str_replace("  ", " ", $line);

           $words = explode(' ', $line);
           if (sizeof($words) > 6) {
               $monw = $words[0];
               $day = $words[1];
               $time = $words[2];
               //Get current year
               $year = date("Y");

               //If this is a starting statement for a request
               if ($words[4] == "web-proxy,account") {
                   global $atime, $ftime, $ip, $url, $size, $rurl;
                   //Clear vars
                   $atime = ""; $ftime = ""; $ip = "";
                   $url = ""; $size = ""; $rurl = "";

                   //Compile useful datetime
                   //Find month number
                   $mon = 0;
                   for ($i = 0; $i < sizeof($months); $i++)
                       if ($months[$i] == $monw) {
                           $mon = $i+1; break;
                       }
                   $mon = "$mon";
                   if (strlen($mon) == 1) $mon = "0".$mon;
                   if (strlen($day) == 1) $day = "0".$day;
                   $atime = "$year-$mon-$day $time";

                   //Client ip
                   $ip = $words[6];
                   #GET (7)
                   #URL (8)
                   $url = $words[8];

                   //Insert partial result
                   if ($this->dbMapper->insLog($atime, '', $url, 0, $ip)) $totRows++;
               } else {
                   //$words[4] == "web-proxy,debug"
                   //Find size
                   $pos = strpos($line, $needle_clen);
                   if ($pos > -1) {
                       //Found size
                       //$size = substr($line, $pos+strlen($needle_clen));
                       $size = substr($line, 73);
                       //echo "found size:$size || ".($pos + strlen($needle_clen))." || ".strlen($line)."\n";
                   }

                   if ($words[6] == "Response") {
                       //Get ftime
                       //Compile useful datetime
                       //Find month number
                       $mon = 0;
                       $rurl = substr($words[9], 0, -2); //Response for url -> must strip ": at the end

                       for ($i = 0; $i < sizeof($months); $i++)
                           if ($months[$i] == $monw) {
                               $mon = $i+1; break;
                           }
                       $mon = "$mon";
                       if (strlen($mon) == 1) $mon = "0".$mon;
                       if (strlen($day) == 1) $day = "0".$day;
                       $ftime = "$year-$mon-$day $time";
                   }

               }
           }


           //Test if all parameters are set
          //echo "<p>atime:$atime, ftime:$ftime, url:$url, size:$size, ip:$ip </p>\n";
           if (($ftime != "") && ($url != "") && ($size != "")) {
               global $atime, $ftime, $ip, $url, $size, $rurl;

               //Update log where return url = .... and ip is .... and ....
               $this->dbMapper->updLog($ftime, $rurl, $size);
               $atime = ""; $ftime = ""; $ip = ""; $url = ""; $size = ""; $rurl = "";
           }
       }
       //Fill empty data to prevent incorrect data extraction
       $this->dbMapper->fillEmptyATime();
       return $totRows;
   }

   public function clearOld($maxDays) {
      return $this->dbMapper->clearOld($maxDays);
   }

   public function ipSize($ip) {
       return $this->dbMapper->ipSize($ip);
   }

   public function ipTime($ip) {
       return $this->dbMapper->ipTime($ip);
   }

   public function allSize($order) {
       return $this->dbMapper->allSize($order);
   }

   public function ipLog($ip, $stime, $etime, $order) {
       return $this->dbMapper->ipLog($ip, $stime, $etime, $order);
   }

   public function normalize() {
       $this->dbMapper->normalize();
   }

}

?>