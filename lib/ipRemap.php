<?php
 class Ip
 {
     static $ipRemap = Array("192.168.0.102" => "Albena",
         "192.168.0.66" => "Milen",
         "192.168.0.118" => "Olia",
         "192.168.0.67" => "Emo",
         "192.168.0.116" => "Maria",
         "192.168.0.113" => "Diliana",
         "192.168.0.111" => "Velislava",
         "192.168.0.107" => "Neli",
         "192.168.0.112" => "Vesi",
         "192.168.0.108" => "Vlado",
         "192.168.0.105" => "Vili",
         "192.168.0.103" => "Aleksiev",
         "192.168.0.115" => "McMaster",
     );

     public static function remapIp($ip)
     {
         if (Ip::$ipRemap[$ip] != null) return Ip::$ipRemap[$ip];
         else return $ip;
     }

 }
?>
