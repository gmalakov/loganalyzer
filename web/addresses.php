<?php
require_once ('../lib/LogAnalyzer.php');
$ip = $_GET["ip"];
$order = $_GET["order"];
if (substr($order,0,1) == " ") $order[0] = "+";

//var_dump($_POST);
$sdate = $_POST["sdate"];
$edate = $_POST["edate"];

if (($sdate == "") && ($edate == "")) {
    $sdate = $_COOKIE["sdate"];
    $edate = $_COOKIE["edate"];
}

$date = new DateTime('now');
if ($sdate == "") $sdate = date_format(date_sub($date, date_interval_create_from_date_string('2 days')), 'Y-m-d');
if ($edate == "") $edate = date("Y-m-d");

setcookie("sdate", $sdate);
setcookie("edate", $edate);

?>
<html>
<head>
    <title>Ip address list</title>
    <meta charset="utf-8" />
    <script src="js/jquery-1.12.4.js"></script>
    <script src="js/jquery-ui.js"></script>

    <script>
        //datepicker({ dateFormat: 'dd-mm-yy' }).val();
        $( function() {
            $( "#sdate" ).datepicker({ dateFormat: 'yy-mm-dd' });
            $( "#edate" ).datepicker({ dateFormat: 'yy-mm-dd' });
        } );

        var corder = "<?php echo $order; ?>";
        function order(ord) {
            if (corder == "+"+ord) corder = "-"+ord;
            else corder = "+"+ord;

            window.location.href = "addresses.php?ip=<?php echo $ip; ?>&order="+corder;
        }
    </script>
</head>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<body>
    <h1>Статистика за адрес <?php echo $ip ?></h1>
    <h2><a href="index.php">Начална страница</a></h2>
    <form name="dates" method="post">
    <table class="iptable addresses">
        <tr><th><a href="javascript:order('date')">Дата</a></th><th>Час</th><th><a href="javascript:order('url')">
                    URL</a></th><th><a href="javascript:order('size')">Трафик</a></th></tr>
        <tr><th colspan="4">
                    От дата:<input type="text" id="sdate" name="sdate" size="30" value="<?php echo $sdate; ?>" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    До дата:<input type="text" id="edate" name="edate" size="30" value="<?php echo $edate; ?>" />

                    <input type="submit" name="submit" value="V" />
            </th></tr>
        <?php
        $analyze = new LogAnalyzer();

        if ($sdate != "") $esdate = $sdate." 00:00:00";
         else $esdate = "";
        if ($edate != "") $eedate = $edate." 23:59:59";
         else $eedate = "";

        $arr = $analyze->ipLog($ip, $esdate,$eedate,$order);
        for ($i = 0; $i < sizeof($arr); $i++)
            if ($arr[$i]["url"] != "") {
                $pos = strpos($arr[$i]["atime"], " ");
                $date = substr($arr[$i]["atime"],0, $pos);
                $time = substr($arr[$i]["atime"], $pos);

                echo "<tr><td>$date</td><td>$time</td><td class='alink'><a target=\"_blank\" ".
                    " href=\"".$arr[$i]["url"]."\" title=\"".$arr[$i]["url"]."\">".
                    substr($arr[$i]["url"],0, 100)."</a></td><td>".hrSize($arr[$i]["size"])."</td>";
            }
        ?>
    </table>
    </form>

</body>
</html>
