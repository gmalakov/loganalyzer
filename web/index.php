<?php
 require_once ('../lib/LogAnalyzer.php');
 require_once ('../lib/ipRemap.php');
 $order = $_GET["order"];
 if (substr($order,0,1) == " ") $order[0] = "+";
 if ($order == "") $order = "+ip";

?>
<html>
 <head>
   <title>Ip address list</title>
   <meta charset="utf-8" />
   <script language="javascript">
       var corder = "<?php echo $order; ?>";
       function order(ord) {
           if (corder == "+"+ord) corder = "-"+ord;
           else corder = "+"+ord;

           window.location.href = "index.php?order="+corder;
       }
   </script>
 </head>
 <link rel="stylesheet" type="text/css" href="css/style.css" />
 <body>
  <table class="iptable">
      <tr><th><a href="javascript:order('ip');">IP</a></th><th><a href="javascript:order('size')">Трафик</a></th><th>Посетени адреси</th></tr>
      <?php
       $analyze = new LogAnalyzer();

       $arr = $analyze->allSize($order);
       for ($i = 0; $i < sizeof($arr); $i++)
           if ($arr[$i]["ip"] != "")
           echo "<tr><td>".Ip::remapIp($arr[$i]["ip"])."</td><td class='right'>".hrSize($arr[$i]["allsize"])."</td>".
//               "<td>".number_format($arr[$i]["alltime"],2,',','')." часа</td>".
               "<td class='ipaddr'><a href=\"addresses.php?ip=".$arr[$i]["ip"]."\">Адреси</a></td></tr>\n";
      ?>
  </table>
 </body>
</html>
